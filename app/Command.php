<?php

declare(strict_types=1);

namespace App;

abstract class Command
{
    protected $bot;
    protected $response;

    public function __construct(Bot $bot, $response)
    {
        $this->bot = $bot;
        $this->response = $response;
    }

    abstract public function call();
}