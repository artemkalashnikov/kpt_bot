<?php

declare(strict_types=1);

namespace App\Command;

use App\Command;

class StartCommand extends Command
{
    public function call()
    {
        $this->bot->sendRequest('sendChatAction', [
            'chat_id' => $this->response['chat']['id'],
            'action' => 'typing'
        ]);

        sleep(1);

        return $this->bot->sendRequest('sendMessage', [
            'chat_id' => $this->response['chat']['id'],
            'text' => 'Привет, это неофициальный бот, который показывает расписание общественного транспорта компании "Киевпастранс"',
            'reply_markup' => json_encode([
                'resize_keyboard' => true,
                'one_time_keyboard' => true,
                'keyboard' => [
                    [['text' => 'Автобус']],
                    [['text' => 'Троллейбус']],
                    [['text' => 'Трамвай']]
                ]
            ])
        ]);
    }
}