<?php

declare(strict_types=1);

namespace App\Command;

use App\Command;
use PHPHtmlParser\Dom;

class TransportDirectionCommand extends Command
{

    private function getUserInfo()
    {
        $redis = $this->bot->getRedis();
        $user = $this->response['from']['id'];

        return json_decode($redis->get($user), true);
    }

    private function parse()
    {
        $dom = new Dom();
        $dom->load($this->getUserInfo()['request']['url']);
        $headers = $dom->find('.col-xs-12.col-sm-6 .name-route');
        $columns = $dom->find('.col-xs-12.col-sm-6');
        $direction = 0;

        if ($this->response['text'] !== trim($headers[0]->text)) {
            $direction += 1;
        }

        $result = $this->response['text'] . "\n\n";

        $time = $columns[$direction]->find('.shift');

        foreach ($time as $row) {
            $timeRow = $row->find('td');
            $timeStart = $timeRow[1]->text;
            $timeEnd = $timeRow[count($timeRow)-1]->text;

            $result .= "$timeStart - $timeEnd\n";
        }

        return $result;
    }

    public function call()
    {
        $this->bot->sendRequest('sendChatAction', [
            'chat_id' => $this->response['chat']['id'],
            'action' => 'typing'
        ]);



        $this->bot->sendRequest('sendMessage', [
            'chat_id' => $this->response['chat']['id'],
            'text' => $this->parse(),
            'reply_markup' => json_encode([
                'resize_keyboard' => true,
                'one_time_keyboard' => true,
                'keyboard' => [
                    [['text' => 'Автобус']],
                    [['text' => 'Троллейбус']],
                    [['text' => 'Трамвай']]
                ]
            ])
        ]);
    }
}