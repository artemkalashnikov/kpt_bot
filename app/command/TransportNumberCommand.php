<?php

declare(strict_types=1);

namespace App\Command;

use App\Command;
use PHPHtmlParser\Dom;

class TransportNumberCommand extends Command
{
    private $transportNumber = '';

    private function getUserInfo()
    {
        $redis = $this->bot->getRedis();
        $user = $this->response['from']['id'];

        return json_decode($redis->get($user), true);
    }

    private function getKeyboard()
    {
        $keyboard = [];
        $dom = new Dom;
        $dom->load($this->getUserInfo()['request']['url']);
        $headers = $dom->find('.col-xs-12.col-sm-6 .name-route');

        foreach ($headers as $header) {
            array_push($keyboard, [['text' => $header->text]]);
        }

        return $keyboard;
    }

    public function saveLastMessage()
    {
        $redis = $this->bot->getRedis();
        $user = $this->response['from']['id'];
        $userInfo = $this->getUserInfo();
        $url = $userInfo['request']['url'];
        $userInfo = [
            'request' => [
                'number' => $this->transportNumber,
                'url' => $url . $this->transportNumber
            ]
        ];

        $redis->set($user, json_encode($userInfo));
    }

    public function setNumber($number)
    {
        $this->transportNumber = $number;
    }

    public function call()
    {
        $this->bot->sendRequest('sendChatAction', [
            'chat_id' => $this->response['chat']['id'],
            'action' => 'typing'
        ]);

        $this->saveLastMessage();

        $this->bot->sendRequest('sendMessage', [
            'chat_id' => $this->response['chat']['id'],
            'text' => 'Направление?',
            'reply_markup' => json_encode([
                'resize_keyboard' => false,
                'one_time_keyboard' => true,
                'keyboard' => $this->getKeyboard()
            ])
        ]);
    }
}