<?php

declare(strict_types=1);

namespace App\Command;

use App\Command;

class HelpCommand extends Command
{
    public function call()
    {
        $this->bot->sendRequest('sendChatAction', [
            'chat_id' => $this->response['chat']['id'],
            'action' => 'typing'
        ]);

        return $this->bot->sendRequest('sendMessage', [
            'chat_id' => $this->response['chat']['id'],
            'text' => 'help'
        ]);
    }
}