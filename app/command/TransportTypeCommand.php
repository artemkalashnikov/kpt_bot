<?php

declare(strict_types=1);

namespace App\Command;

use App\Bot;
use App\Command;
use PHPHtmlParser\Dom;

class TransportTypeCommand extends Command
{
    private $url = 'https://kpt.kyiv.ua/schedule/';

    private function getKeyboard()
    {
        $keyboard = [];
        $dom = new Dom;
        $dom->load($this->url);
        $links = $dom->find('a.transport-link');

        for($i = 0; $i < count($links); $i++) {

            $number = preg_split('~/~', $links[$i]->getAttribute('href'));
            array_push($keyboard, ['text' => urldecode($number[3])]);
        }

        return array_chunk($keyboard, 6);
    }

    public function setTransport($transport)
    {
        $this->url .= $transport;
    }

    public function saveLastMessage()
    {
        $redis = $this->bot->getRedis();
        $user = $this->response['from']['id'];
        $userInfo = [
            'is_bot' => $user['is_bot'],
            'first_name' => $user['first_name'],
            'request' => [
                'transport' => $this->response['text'],
                'url' => $this->url . '/'
            ]
        ];

        if (isset($user['last_name'])) {
            $userInfo['last_name'] = $user['last_name'];
        }

        if (isset($user['username'])) {
            $userInfo['username'] = $user['username'];
        }

        if (isset($user['language_code'])) {
            $userInfo['language_code'] = $user['language_code'];
        }

        $redis->set($user, json_encode($userInfo));
    }

    public function call()
    {
        $this->bot->sendRequest('sendChatAction', [
            'chat_id' => $this->response['chat']['id'],
            'action' => 'typing'
        ]);

        $this->saveLastMessage();

        $this->bot->sendRequest('sendMessage', [
            'chat_id' => $this->response['chat']['id'],
            'text' => 'Номер транспорта?',
            'reply_markup' => json_encode([
                'resize_keyboard' => false,
                'one_time_keyboard' => true,
                'keyboard' => $this->getKeyboard()
            ])
        ]);
    }
}