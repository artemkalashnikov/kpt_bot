<?php

declare(strict_types=1);

namespace App;

use Predis\Client;

class Bot
{
    private $token;
    private $hookUrl;
    private $redis;
    private $telegramUrl = 'https://api.telegram.org/bot';
    private $response = [];

    public static function create(string $token, string $hookUrl, array $redis)
    {
        return new self($token, $hookUrl, $redis);
    }

    private function __construct(string $token, string $hookUrl, array $redis)
    {
        $this->token = $token;
        $this->hookUrl = $hookUrl;
        $this->redis = new Client($redis);
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getHookUrl()
    {
        return $this->hookUrl;
    }

    public function getRedis()
    {
        return $this->redis;
    }

    public function sendRequest(string $method, array $parameters = [])
    {
        return file_get_contents($this->telegramUrl . $this->getToken() . "/$method?" . http_build_query($parameters));
    }

    public function listen(): array
    {
        $response = json_decode(file_get_contents("php://input"), true);

        if (isset($response['message'])) {
            $this->response = $response['message'];
            return $response['message'];
        }
//        elseif (isset($response['edited_message'])) {
//            $this->response = $response['edited_message'];
//            return $response['edited_message'];
//        }

        throw new \Error('Something went wrong.');
    }

    public function command(string $pattern, callable $next)
    {
        if (preg_match('~' . $pattern . '~iu', $this->response['text'], $matches)) {
            if (isset($matches[0])) {
                $next($this, $matches[0]);
            } else {
                $next($this);
            }
        }
    }
}