<?php

declare(strict_types=1);

use App\Bot;
use App\Command\TransportDirectionCommand;
use App\Command\TransportNumberCommand;
use App\Command\TransportTypeCommand;
use App\Command\StartCommand;
use App\Command\HelpCommand;

require __DIR__ . '/vendor/autoload.php';

$config = include('settings.php');
$bot = Bot::create($config['token'], $config['hookUrl'], $config['redis']);

$response = $bot->listen();

$bot->command('/start', function(Bot $bot) use ($response) {
    $command = new StartCommand($bot, $response);
    $command->call();
});

$bot->command('/help', function(Bot $bot) use ($response) {
    $command = new HelpCommand($bot, $response);
    $command->call();
});

$bot->command('^автобус|bus$', function(Bot $bot) use ($response) {
    $command = new TransportTypeCommand($bot, $response);
    $command->setTransport('bus');
    $command->call();
});

$bot->command('^троллейбус|тролейбус|trolleybus$', function(Bot $bot) use ($response) {
    $command = new TransportTypeCommand($bot, $response);
    $command->setTransport('trolley');
    $command->call();
});

$bot->command('^трамвай|tram$', function(Bot $bot) use ($response) {
    $command = new TransportTypeCommand($bot, $response);
    $command->setTransport('tram');
    $command->call();
});

$bot->command('^[0-9]{1,3}[a-zA-Z\p{L}]{0,2}$', function(Bot $bot, $number) use ($response) {
    $command = new TransportNumberCommand($bot, $response);
    $command->setNumber($number);
    $command->call();
});

$bot->command(' - ', function (Bot $bot) use ($response) {
    $command = new TransportDirectionCommand($bot, $response);
    $command->call();
});
