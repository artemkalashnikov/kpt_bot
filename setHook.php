<?php

declare(strict_types=1);

use App\Bot;

require __DIR__ . '/vendor/autoload.php';

$config = include('settings.php');

$bot = Bot::create($config['token'], $config['hookUrl'], $config['redis']);

$bot->sendRequest('setWebhook', [
    'url' => $bot->getHookUrl()
]);

$result = $bot->sendRequest('getWebhookInfo');

echo $result;

