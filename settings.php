<?php

return [
    'token' => $_ENV['TELEGRAM_TOKEN'],
    'hookUrl' => 'https://guarded-ravine-40161.herokuapp.com/index.php',
    'redis' => [
        'host' => parse_url($_ENV['REDISCLOUD_URL'], PHP_URL_HOST),
        'port' => parse_url($_ENV['REDISCLOUD_URL'], PHP_URL_PORT),
        'password' => parse_url($_ENV['REDISCLOUD_URL'], PHP_URL_PASS),
    ]
];